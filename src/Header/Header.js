import React, {Component} from "react";
import './Header.css'

class Header extends Component {
    render() {
        return (
            <div className="header">
                <div className="container">
                    <Logo />
                    <Contacts />
                </div>
            </div>
        )
    }
}

class Logo extends Component {
    render() {
        return (
            <div className="logo">
                <div className="form">
                    <div className="left">
                        <span className="poly" />
                        <span />
                        <span className="poly" />
                    </div>
                    <span />
                    <div className="right">
                        <span className="poly" />
                        <span />
                        <span className="poly" />
                    </div>
                </div>
                <div className="text">Ceiling</div>
            </div>
        )
    }
}

class Contacts extends Component {
    render() {
        return (
            <div className="phone">
                <div className="block">
                    <a href="tel:+79518354989">
                        <span className="call"><span className="icon" />+7 (951) 835-49-89</span>
                    </a>
                    <input type="button" value="Заказать замер"/>
                </div>
            </div>
        )
    }
}

export default Header