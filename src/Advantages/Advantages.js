import React, { Component, Fragment } from 'react'
import './Advantages.css'

class Advantages extends Component {
    render() {
        return (
            <div className="advantages">
                <Advantage />
            </div>
        )
    }
}

class Advantage extends Component {
    render() {
        return (
            <Fragment>
                <span className="title">
                    Преимущества, которые вы получаете.
                </span>
                <div className="bricks">
                    <div className="brick">
                        <span className="content">
                            <span className="title">
                                Преимущество 1
                            </span>
                            <span className="description">
                                Детальное описание очень крутого первого преимущеста
                            </span>
                        </span>
                    </div>
                    <div className="brick">
                        <span className="content">
                            <span className="title">
                                Преимущество 2
                            </span>
                            <span className="description">
                                Детальное описание очень крутого второго преимущеста
                                Детальное описание очень крутого второго преимущеста
                            </span>
                        </span>
                    </div>
                    <div className="brick">
                        <span className="content">
                            <span className="title">
                                Преимущество 3
                            </span>
                            <span className="description">
                                Детальное описание очень крутого третьего преимущеста
                                Детальное описание очень крутого третьего преимущеста
                                Детальное описание очень крутого третьего преимущеста
                            </span>
                        </span>
                    </div>
                    <div className="brick">
                        <span className="content">
                            <span className="title">
                                Преимущество 4
                            </span>
                            <span className="description">
                                Детальное описание очень крутого четвертого преимущеста
                                Детальное описание очень крутого четвертого преимущеста
                                Детальное описание очень крутого четвертого преимущеста
                            </span>
                        </span>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default Advantages