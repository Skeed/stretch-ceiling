import React, { Component } from 'react'
import './Slides.css'
import slide2 from "../images/slider/slide2.jpg";
import $ from "jquery";

class Slides extends Component {
    render() {
        return (
            <div className="recommended">
                <SlideShow image={slide2}/>
            </div>
        )
    }
}

class SlideShow extends Component {
    static handleClick() {
        $('html, body').animate({scrollTop: ($('#cost')[0].getBoundingClientRect().top + pageYOffset)}, 800);
    }

    render() {
        return(
            <div className="image">
                <div className="text">
                    <span>
                    Натяжные потолки <br/>
                    по цене от <br/>
                    169 руб. кв<sup>2</sup>
                    </span>
                </div>
                <input type="button" value="Расчитать стоимость" onClick={SlideShow.handleClick.bind(this)}/>
                <div className="figure" />
                <div className="back" />
                <div className="slide" />
            </div>
        )
    }
}

export default Slides