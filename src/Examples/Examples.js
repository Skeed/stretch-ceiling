import React, { Component} from 'react'
import './Examples.css'
import inter1 from '../images/inter/inter1.jpg'
import inter2 from '../images/inter/inter2.jpg'
import inter3 from '../images/inter/inter3.jpg'
import inter4 from '../images/inter/inter4.jpg'
import inter5 from '../images/inter/inter5.jpg'
import inter6 from '../images/inter/inter6.jpg'

class Examples extends Component {
    render() {
        return (
            <div className="examples">
                <span className="title">
                    Примеры наших работ
                </span>
                <div className="blocks">
                    <div>
                        <div className="block">
                            <img className="image" src={inter2} />
                            <span className="title">Фото галереи 1</span>
                        </div>
                        <div className="block">
                            <img className="image" src={inter5} />
                            <span className="title">Фото галереи 4</span>
                        </div>
                    </div>
                    <div>
                        <div className="block">
                            <img className="image" src={inter1} />
                            <span className="title">Фото галереи 2</span>
                        </div>
                        <div className="block">
                            <img className="image" src={inter4} />
                            <span className="title">Фото галереи 5</span>
                        </div>
                    </div>
                    <div>
                        <div className="block">
                            <img className="image" src={inter6} />
                            <span className="title">Фото галереи 3</span>
                        </div>
                        <div className="block">
                            <img className="image" src={inter3} />
                            <span className="title">Фото галереи 6</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Examples