import React, { Component } from 'react'
import './Footer.css'
import $ from "jquery";

class Footer extends Component {
    static handleClick() {
        $('html, body').animate({scrollTop: ($('#cost')[0].getBoundingClientRect().top + pageYOffset)}, 800);
    }

    render() {
        return (
            <ul className="footer">
                <li>
                    <ul className="text">
                        <li>
                            Phone : 8 (951) 835-49-89
                        </li>
                        <li>
                            © 2019 - Ceiling | Компания натяжных потолков
                        </li>
                    </ul>
                </li>
                <li>
                    <input type="button" value="Расчитать стоимость" onClick={Footer.handleClick.bind(this)}/>
                    <input className="blue" type="button" value="Заказать замер"/>
                </li>
            </ul>
        )
    }
}


export default Footer