import React, { Component, Fragment } from 'react'
import icon_smartphone from '../images/smartphone.svg'
import icon_maps from '../images/maps.svg'
import icon_home from '../images/home.svg'
import './Levels.css'

class Levels extends Component {
    render() {
        return (
            <div className="levels">
                <span className="title">Как это работает</span>
                <ul>
                    <Level
                        icon={icon_smartphone}
                        title="Замер помещения"
                        content="Тем не менее стоит отметить так же, что новая модель."
                    />
                    <LineDots />
                    <Level
                        icon={icon_maps}
                        title="Выбор дизайна"
                        content="С другой стороны рамки и место обучения кадров обеспечивает широкому кругу."
                    />
                    <LineDots />
                    <Level
                        icon={icon_home}
                        title="Постройка потолка"
                        content="С другой стороны движение в данном направлении позволяет выполнить важные задания"
                    />
                </ul>
                <input type="button" value="Заказать замер"/>
            </div>
        )
    }
}

class Level extends Component {
    render() {
        return (
            <li>
                <div className="circle">
                    <img src={this.props.icon}/>
                </div>
                <span className="topic">
                    {this.props.title}
                </span>
                <span className="content">
                    {this.props.content}
                </span>
            </li>
        )
    }
}

class LineDots extends Component {
    render() {
        return (
            <li className="block-dots">
                <span className="dots">

                </span>
            </li>
        )
    }
}

export default Levels