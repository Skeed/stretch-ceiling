import React, { Component} from 'react'
import $ from 'jquery'
import './TestCost.css'

class TestCost extends Component {
    constructor(props) {
        super(props);

        this.state = {
            square: 1,
            angles: 4,
            luster: 1,
            dots: 1,
            type: "glossy",
        }
    }

    handleChange(event) {
        let value = event.target.value;
        let name = event.target.name;

        if (value === '') {
            value = name === 'angles' ? 4 : 1;
        }

        if (value > 999) {
            value = 999;
        }

        if (value < 1) {
            value = 1;
        }

        if (name === 'angles' && value < 4) {
            value = 4;
        }

        this.setState({
            [name]: value,
        })
    }

    handleCalculate() {
        let cost = 0;
        let costPerMeter = 200;

        switch (this.state.type) {
            case 'glossy':
                costPerMeter = 200;
                break;
            case 'matte':
                costPerMeter = 400;
                break;
            case 'satin':
                costPerMeter = 600;
        }

        cost +=
            this.state.square * costPerMeter +
            this.state.angles * 150 +
            this.state.luster * 350 +
            this.state.dots * 300
        ;

        $('#costText').html('Примерная стоимость потолка: ' + cost + ' руб.');
    }

    render() {
        return (
            <div id="cost" className="test-cost">
                <span className="title">
                    Расчитать стоимость
                </span>
                <ul>
                    <li className="inputs">
                        <span>
                            <label>
                                <span className="label">Площадь</span>
                                <input name="square" value={this.state.square} onChange={this.handleChange.bind(this)} type="number"/>
                            </label>
                        </span>
                        <span>
                            <label>
                                <span className="label">Кол-во углов</span>
                                <input name="angles" value={this.state.angles} onChange={this.handleChange.bind(this)} type="number"/>
                            </label>
                        </span>
                        <span>
                            <label>
                                <span className="label">Кол-во люстр</span>
                                <input name="luster" value={this.state.luster} onChange={this.handleChange.bind(this)} type="number"/>
                            </label>
                        </span>
                        <span>
                            <label>
                                <span className="label">Кол-во точек</span>
                                <input name="dots" value={this.state.dots} onChange={this.handleChange.bind(this)} type="number"/>
                            </label>
                        </span>
                        <span>
                            <label>
                                <span className="label">Тип</span>
                                <select name="type" value={this.state.type} onChange={this.handleChange.bind(this)} >
                                    <option selected value="glossy">Глянцевый</option>
                                    <option value="matte">Матовый</option>
                                    <option value="satin">Сатин</option>
                                </select>
                            </label>
                        </span>
                    </li>
                    <li className="data">
                        <div className="button-test" onClick={this.handleCalculate.bind(this)}>Расчитать</div>
                        <div id="costText" className="cost">Кликните кнопку расчитать стоимость</div>
                        <div className="button-test">Заказать замер</div>
                    </li>
                </ul>
            </div>
        )
    }
}

export default TestCost