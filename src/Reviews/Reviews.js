import React, { Component} from 'react'
import './Reviews.css'
import conf1 from '../images/reviews/conf1.png'
import conf2 from '../images/reviews/conf2.png'
import conf3 from '../images/reviews/conf3.png'
import conf4 from '../images/reviews/conf4.png'

class Reviews extends Component {
    render() {
        return (
            <div className="reviews">
                <span className="title">
                    Отзывы наших клиентов
                </span>
                <div className="blocks">
                    <Review
                        image={conf1}
                        name="Рябов Виль"
                        inst="@rainbowsalt"
                        review="Сомнение рефлектирует естественный закон исключённого третьего.. Дедуктивный метод решительно представляет собой бабувизм. Интеллек."
                    />
                    <Review
                        image={conf3}
                        name="Константинова Ульяна"
                        inst="@coyoteflowers"
                        review="Структурализм абстрактен. Созерцание непредсказуемо. Созерцание непредсказуемо. Отсюда естественно следует, что автоматизация диск."
                    />
                    <Review
                        image={conf2}
                        name="Спивак Ульяна"
                        inst="@fuckyoumakeart"
                        review="Надстройка нетривиальна. Надстройка нетривиальна. Смысл жизни, следовательно, творит данный закон внешнего мира. Сомнение рефлект."
                    />
                    <Review
                        image={conf4}
                        name="Одинцов Жерар"
                        inst="@loversland"
                        review="Импликация, следовательно, контролирует бабувизм, открывая новые горизонты. Апостериори, гравитационный парадокс амбивалентно понимает под собой интеллигибельный."
                    />
                </div>
            </div>
        )
    }
}

class Review extends Component {
    render() {
        return (
            <div className="review">
                <span className="circle">
                    <img className="image" src={this.props.image} alt="cool" />
                </span>
                <span className="title">
                    {this.props.name}
                </span>
                <span className="profession">
                    {this.props.inst}
                </span>
                <span className="description">
                    {this.props.review}
                </span>
            </div>
        )
    }
}

export default Reviews