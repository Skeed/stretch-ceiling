import React, { Component } from 'react'
import './Materials.css'
import $ from 'jquery'
import texture1 from '../images/texture/texture.jpg'
import texture2 from '../images/texture/changed-texture.jpg'
import texture3 from '../images/texture/changed-texture-2.jpg'

class Materials extends Component {
    render() {
        return (
            <div className="materials">
                <Material
                    image={texture1}
                    title="Специальная ткань"
                    description="Не следует, однако, забывать, что общая концепция сложившегося положения в значительной степени обусловливает создание соответствующих моделей активизации.
Таким образом общая концепция сложившегося положения ни коим образом не противоречит нашей теории, а скорее наоборот, способствует улучшению идей по выходу из сложившейся ситуации.
Не следует, однако, забывать, что движение в данном направлении способствует подготовке и реализации идей по выходу из сложившейся ситуации.
Товарищи, начало повседневной работы по формированию позиции обеспечивает широкому кругу (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных задач."
                />
                <Material
                    direction="right"
                    image={texture2}
                    title="Специальный камень"
                    description="Не следует, однако, забывать, что общая концепция сложившегося положения в значительной степени обусловливает создание соответствующих моделей активизации.
Таким образом общая концепция сложившегося положения ни коим образом не противоречит нашей теории, а скорее наоборот, способствует улучшению идей по выходу из сложившейся ситуации.
Не следует, однако, забывать, что движение в данном направлении способствует подготовке и реализации идей по выходу из сложившейся ситуации."
                />
                <Material
                    image={texture3}
                    title="Специальная кладка"
                    description="Не следует, однако, забывать, что общая концепция сложившегося положения в значительной степени обусловливает создание соответствующих моделей активизации.
Таким образом общая концепция сложившегося положения ни коим образом не противоречит нашей теории, а скорее наоборот."
                />
            </div>
        )
    }
}

class Material extends Component {
    constructor(props){
        super(props);

        this.slide =
            <li className="slide">
                <div className="storage">
                    <img id="text1" src={this.props.image} alt="texture1"/>
                </div>
            </li>;

        this.description =
            <li className="description">
                    <span className="title">
                        {this.props.title}
                    </span>
                <span className="text">
                        {this.props.description}
                    </span>
            </li>;
    }
    render() {
        if (!this.props.direction || this.props.direction === 'left') {
            return (
                <ul className="material">
                    {this.description}
                    {this.slide}
                </ul>
            );
        } else {
            return (
                <ul className="material">
                    {this.slide}
                    {this.description}
                </ul>
            );
        }
    }
}

export default Materials