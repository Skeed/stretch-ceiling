import React, {Component, Fragment} from 'react';
import Header from './Header/Header'
import Slides from './Slides/Slides'
import Advantages from './Advantages/Advantages'
import Materials from "./Materials/Materials";
import Levels from './Levels/Levels'
import Reviews from "./Reviews/Reviews";
import TestCost from './TestCost/TestCost'
import Examples from "./Examples/Examples";
import Footer from './Footer/Footer'
import $ from "jquery";
import './App.css';

window.onload = function() {
    if (pageYOffset > window.innerHeight) {
        $('#buttonUp').show();
    } else {
        $('#buttonUp').hide();
    }
};

window.addEventListener('scroll', function() {
    if (pageYOffset > window.innerHeight) {
        $('#buttonUp').fadeIn('slow');
    } else {
        $('#buttonUp').fadeOut('slow');
    }
    let yOffset = $('.material')[0].getBoundingClientRect().top + ($('.material')[0].getBoundingClientRect().bottom - $('.material')[0].getBoundingClientRect().top) / 1.1;
    if (pageYOffset > yOffset) {
        $('#text1').addClass("active");
        $('.shadow').addClass("active");
        $('.title').addClass("active");
        $('.text').addClass("active");
    }
});

class App extends Component {
  render() {
    return (
        <Fragment>
            <Header />
            <Slides />
            <Advantages />
            <Levels />
            <Materials />
            <Reviews />
            <TestCost />
            <Examples />
            <Footer />
            <ButtonUp />
        </Fragment>
    );
  }
}

class ButtonUp extends Component {
    static handleClick () {
        $('html, body').animate({scrollTop: '0px'}, 800);
    }

    render() {
        return (
            <div id="buttonUp" className="button-up" onClick={ButtonUp.handleClick.bind(this)}>
                <span className="icon-up" />
            </div>
        )
    }
}

export default App;
